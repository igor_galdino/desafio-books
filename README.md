## Desafio books frontend

Esse projeto foi um desafio lançado pela Ioasys

## Sobre aplicação
A aplicação consiste em um catálogos de livros. O usuário poderá se autenticar através de uma tela de login. Quando o usuário efetuar seu login, ele é redirecionado para uma lista de livros. Nesta lista, o usuário pode selecionar um livro e visualizar o detalhamento do mesmo.


### Tecnologias utilizadas
* ReactJS
* NextJs
* Axios
* Eslint
* Prettier
* Material-ui
* Styled-components

### Como rodar a aplicação
#### Instalar as dependências:
``` yarn ou yarn install```

#### Executar linter:
``` yarn lint ```

#### Iniciar servidor:
``` yarn dev ```
