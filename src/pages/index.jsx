import React, { createContext, useEffect, useState } from "react";
import { useRouter } from "next/router";
import Cookies from "js-cookie";
import { IconButton, Typography } from "@mui/material";
import {
  HomeContainer,
  HomeContent,
  Header,
} from "../styles/pages/home.styles";
import LoginService from "../services/LoginService";
import Books from "../components/Books/Books";

export const AppContext = createContext({});

const Home = () => {
  const router = useRouter();
  const [user, setUser] = useState("");

  useEffect(() => {
    const token = Cookies.get("access_token");
    if (!token) {
      router.replace("/login");
    } else {
      setUser(LoginService.getUserName());
    }
  }, [router]);

  const logout = () => {
    LoginService.logout();
    router.replace("/login");
  };

  return (
    <HomeContainer>
      <HomeContent maxWidth="xl">
        <Header>
          <Typography variant="h5">
            <b>ioasys</b> Books
          </Typography>
          <div>
            <span>
              Bem vindo, <b>{user.split(" ").shift() ?? "Guilherme"}!</b>
            </span>
            <IconButton aria-label="logout" size="small" onClick={logout}>
              <img src="/img/icons/exit.png" alt="Logout" width={32} />
            </IconButton>
          </div>
        </Header>
        <Books />
      </HomeContent>
    </HomeContainer>
  );
};

export default Home;
