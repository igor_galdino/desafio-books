import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import {
  ClickAwayListener,
  FilledInput,
  InputAdornment,
  InputLabel,
  Tooltip,
} from "@mui/material";
import {
  ButtonLogin,
  LoginForm,
  Header,
  Input,
  InputPassword,
  LoginContainer,
  ErroMessage,
} from "../styles/pages/login.styles";
import LoginService from "../services/LoginService";
import AuthService from "../services/AuthService";

const Login = () => {
  const router = useRouter();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [open, setOpenTooltip] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  useEffect(() => {
    if (AuthService.getAccessToken()) {
      router.push("/");
    }
  }, [router]);

  const login = async () => {
    try {
      const resp = await LoginService.login(email, password);
      if (resp) {
        LoginService.setUserName(resp.name);
        router.push("/");
      }
    } catch (error) {
      setOpenTooltip(true);
      setErrorMessage("Email e/ou senha incorretos.");
    }
  };

  const handleTooltipClose = () => {
    setOpenTooltip(false);
  };

  return (
    <LoginContainer>
      <LoginForm>
        <Header>
          <b>Ioasys</b> Books
        </Header>
        <Input
          label="Email"
          variant="filled"
          onChange={(event) => setEmail(event.target.value)}
        />
        <ClickAwayListener onClickAway={handleTooltipClose}>
          <Tooltip
            arrow
            open={open}
            onClose={handleTooltipClose}
            disableFocusListener
            disableHoverListener
            disableTouchListener
            title={<ErroMessage>{errorMessage}</ErroMessage>}
            placement="bottom-start"
          >
            <InputPassword variant="filled">
              <InputLabel htmlFor="password">Senha</InputLabel>
              <FilledInput
                id="password"
                label="Password"
                type="password"
                onChange={(event) => setPassword(event.target.value)}
                endAdornment={
                  <InputAdornment position="end">
                    <ButtonLogin variant="container" onClick={login}>
                      Entrar
                    </ButtonLogin>
                  </InputAdornment>
                }
              />
            </InputPassword>
          </Tooltip>
        </ClickAwayListener>
      </LoginForm>
    </LoginContainer>
  );
};

export default Login;
