import "../styles/globals.css";

import React from "react";
import { ThemeProvider } from "@mui/material";
import theme from "../themes/theme";

function MyApp({ Component, pageProps }) {
  return (
    <ThemeProvider theme={theme}>
      <Component {...pageProps} />
    </ThemeProvider>
  );
}

export default MyApp;
