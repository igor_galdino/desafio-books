import { createTheme } from "@mui/material/styles";
import { ptBR } from "@mui/material/locale";

const theme = createTheme(
  {
    palette: {
      primary: {
        light: "#9661ff",
        main: "#B22E6F", // Change
        dark: "#000000", // Change
      },
      text: {
        primary: "#000000", // Change
        secondary: "#9B9B9B",
        author: "#AB2680", // Change
      },
      error: {
        main: "#FC3C00",
      },
      warning: {
        main: "#FCA600",
      },
      success: {
        main: "#00D34D",
      },
      black: {
        200: "#00000066",
      },
      grey: {
        50: "#FAFAFA",
        100: "#F0F0F0",
        200: "#D7D9DD",
        300: "#C4C4C4",
        400: "#9B9B9B",
      },
    },
    typography: {
      fontFamily: "Inter",
    },
    shape: {
      borderRadius: "3px",
    },
    components: {
      MuiButton: {
        styleOverrides: {
          root: {
            textTransform: "none",
          },
        },
        variants: [
          {
            props: { variant: "contained", color: "secondary" },
            style: {
              color: "white",
            },
          },
        ],
      },
      MuiPaper: {
        styleOverrides: {
          root: {
            boxShadow: "0px 0px 39px rgba(0, 0, 0, 0.05)",
          },
        },
      },
    },
  },
  ptBR
);

export default theme;
