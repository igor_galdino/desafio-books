import { styled } from "@mui/material/styles";
import { Container } from "@mui/material";

export const HomeContainer = styled("div")`
  min-width: 100vh;
  min-height: 100vh;
  background-image: url("/img/homeBackground2.png"),
    url("/img/homeBackground1.png");
  background-repeat: no-repeat;
  background-size: cover;
`;

export const HomeContent = styled(Container)`
  display: flex;
  flex-direction: column;
  gap: ${({ theme }) => theme.spacing(7)};

  ${({ theme }) => theme.breakpoints.down("sm")} {
    align-items: center;
  }
`;

export const Header = styled("header")`
  display: flex;
  justify-content: space-between;
  align-items: center;

  margin-top: ${({ theme }) => theme.spacing(2)};

  div > * {
    margin: 0 ${({ theme }) => theme.spacing(1)};
    font-size: ${({ theme }) => theme.spacing(1.5)};
  }

  .MuiTypography-root {
    font-size: ${({ theme }) => theme.spacing(3.5)};
    b {
      font-size: ${({ theme }) => theme.spacing(4)};
    }
  }

  ${({ theme }) => theme.breakpoints.down("sm")} {
    span {
      display: none;
    }
    justify-content: start;
    grid-gap: ${({ theme }) => theme.spacing(8)};
  }
`;
