import { styled } from "@mui/material/styles";
import { Button, Container, FormControl, TextField } from "@mui/material";

export const LoginContainer = styled("div")`
  width: 100vw;
  height: 100vh;
  background-image: url("/img/background.png");
  background-repeat: no-repeat;
  background-size: cover;

  color: ${({ theme }) => theme.palette.grey[50]};

  display: flex;
`;

export const LoginForm = styled(Container)`
  display: flex;
  flex-direction: column;
  gap: ${({ theme }) => theme.spacing(2)};

  justify-content: center;

  margin-left: ${({ theme }) => theme.spacing(20)};

  ${({ theme }) => theme.breakpoints.down("sm")}{
    margin-left: ${({ theme }) => theme.spacing(2)};
    align-items: center;
  }

  label {
    color: ${({ theme }) => theme.palette.text.secondary} !important;
  }
`;

export const Header = styled("header")`
  font-size: ${({ theme }) => theme.spacing(4)};
`;

export const Input = styled(TextField)`
  width: ${({ theme }) => theme.spacing(50)};
  background-color: ${({ theme }) => theme.palette.black[200]};
  color: white;
  .MuiInputBase-input {
    color: white;
  }

  ${({ theme }) => theme.breakpoints.down("sm")}{
    width: ${({ theme }) => theme.spacing(30)};
  }
`;

export const InputPassword = styled(FormControl)`
  width: ${({ theme }) => theme.spacing(50)};
  background-color: ${({ theme }) => theme.palette.black[200]};
  .MuiInputBase-input {
    color: white;
  }

  ${({ theme }) => theme.breakpoints.down("sm")}{
    width: ${({ theme }) => theme.spacing(30)};
  }
`;

export const ButtonLogin = styled(Button)`
  background-color: ${({ theme }) => theme.palette.grey[50]};
  border-radius: ${({ theme }) => theme.spacing(3)};
  font-size: ${({ theme }) => theme.spacing(2)};
  font-weight: 600;
  color: ${({ theme }) => theme.palette.primary.main};
  &:hover {
    background-color: ${({ theme }) => theme.palette.grey[100]};
  }

  ${({ theme }) => theme.breakpoints.down("sm")}{
    font-size: ${({ theme }) => theme.spacing(1.5)};
    width: ${({ theme }) => theme.spacing(6)};
  }
`;

export const ErroMessage = styled("h2")`
  color: white;
  padding: ${({ theme }) => theme.spacing(2)};
`;
