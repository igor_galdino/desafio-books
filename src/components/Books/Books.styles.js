import { styled } from "@mui/material/styles";
import { Modal, TablePagination } from "@mui/material";

export const BooksContainer = styled("div")`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  gap: ${({ theme }) => `${theme.spacing(4)} ${theme.spacing(7)}`};

  ${({ theme }) => theme.breakpoints.down("lg")} {
    grid-template-columns: repeat(3, 1fr);
  }

  ${({ theme }) => theme.breakpoints.down("md")} {
    grid-template-columns: repeat(2, 1fr);
  }

  ${({ theme }) => theme.breakpoints.down("sm")} {
    grid-template-columns: 1fr;
  }
`;

export const Pagination = styled(TablePagination)`
  margin-top: ${({ theme }) => theme.spacing(1)};
  .MuiTablePagination-actions {
    & > * {
      border: 1px solid ${({ theme }) => theme.palette.text.secondary};
      margin: 0 0.2rem;
    }
  }
`;

export const ModalContainer = styled(Modal)`
  display: flex;
  justify-content: center;
  align-items: center;
`;
