import React, { useState } from "react";
import { Box } from "@mui/material";
import { BooksContainer, Pagination } from "./Books.styles";
import BookModal from "../BookModal/BookModal";
import BookContent from "../BookContent/BookContent";
import BookNoContent from "../BookNoContent/BookNoContent";

const Books = () => {
  const [page, setPage] = useState(0);
  const [totalItems, setTotalItems] = useState(0);
  const [selectedBookId, setSelectedBookId] = useState(null);
  const [open, setOpen] = useState(false);
  const AMOUNT = 12;

  const openModal = () => setOpen(true);
  const closeModal = () => setOpen(false);

  const handleChangePage = (_, newPage) => {
    setPage(newPage);
  };

  const selectBook = (bookId) => {
    setSelectedBookId(bookId);
    openModal();
  };

  return (
    <>
      <Box>
        <BooksContainer>
          <BookContent
            page={page}
            amount={AMOUNT}
            selectBook={selectBook}
            setTotalItems={setTotalItems}
          />
        </BooksContainer>
        {totalItems > 0 ? (
          <Pagination
            rowsPerPageOptions={[]}
            component="div"
            count={totalItems}
            rowsPerPage={AMOUNT}
            page={page}
            onPageChange={handleChangePage}
          />
        ) : (
          <BookNoContent />
        )}
      </Box>
      {selectedBookId && (
        <BookModal
          bookId={selectedBookId}
          open={open}
          handleClose={closeModal}
        />
      )}
    </>
  );
};

export default Books;
