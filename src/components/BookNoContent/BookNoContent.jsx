import React from "react";
import { NoContentContainer, NoContentText } from "./BookNoContent.styles";

const BookNoContent = () => (
  <NoContentContainer>
    <NoContentText variant="body1">Sem livros cadastrados no sistema</NoContentText>
  </NoContentContainer>
);

export default BookNoContent;
