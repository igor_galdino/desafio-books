import { styled } from "@mui/material/styles";
import { Container, Typography } from "@mui/material";

export const NoContentContainer = styled(Container)`
  text-align: center;
  border: 1px solid ${({theme}) => theme.palette.text.secondary};
  margin-top: ${({theme}) => theme.spacing(2)};
`;

export const NoContentText = styled(Typography)`
  font-size: ${({theme}) => theme.spacing(3)};
`;
