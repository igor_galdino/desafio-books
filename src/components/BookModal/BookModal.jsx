import React, { useEffect, useState } from "react";
import { Grid, Typography } from "@mui/material";
import {
  BookAuthors,
  BookDescription,
  BookImg,
  BookInfo,
  BookTitle,
  CloseIcon,
  ModalContainer,
  ModalContent,
} from "./BookModal.styles";
import BookService from "../../services/BookService";

const BookModal = (props) => {
  const { bookId, open, handleClose } = props;
  const [book, setBook] = useState(null);

  const findBookById = async (id) => {
    const resp = await BookService.findBookById(id);
    setBook(resp);
  };

  useEffect(() => {
    findBookById(bookId);
  }, [bookId]);

  return (
    <>
      {book && (
        <>
          <ModalContainer
            open={open}
            onClose={handleClose}
            aria-labelledby="Titulo do modal"
            aria-describedby="Descrições do livro selecionado"
          >
            <ModalContent>
              <BookImg src={book.imageUrl} alt={book.title} />
              <div>
                <CloseIcon
                  src="/img/icons/close.png"
                  aria-label="Fechar modal"
                  onClick={handleClose}
                />
                <BookTitle variant="h4">{book.title}</BookTitle>
                <BookAuthors variant="body2" color="text.author">
                  {book.authors.join(", ")}
                </BookAuthors>
              </div>
              <BookInfo>
                <Typography variant="h6">Informações</Typography>
                <Grid container>
                  <Grid container>
                    <Grid item xs={6}>
                      Páginas
                    </Grid>
                    <Grid item xs={6}>
                      {book.pageCount}
                    </Grid>
                  </Grid>
                  <Grid container>
                    <Grid item xs={6}>
                      Editora
                    </Grid>
                    <Grid item xs={6}>
                      {book.publisher}
                    </Grid>
                  </Grid>
                  <Grid container>
                    <Grid item xs={6}>
                      Publicação
                    </Grid>
                    <Grid item xs={6}>
                      {book.published}
                    </Grid>
                  </Grid>
                  <Grid container>
                    <Grid item xs={6}>
                      Idioma
                    </Grid>
                    <Grid item xs={6}>
                      {book.language}
                    </Grid>
                  </Grid>
                  <Grid container>
                    <Grid item xs={6}>
                      Título Original
                    </Grid>
                    <Grid item xs={6}>
                      {book.title}
                    </Grid>
                  </Grid>
                  <Grid container>
                    <Grid item xs={6}>
                      ISBN-10
                    </Grid>
                    <Grid item xs={6}>
                      {book.isbn10}
                    </Grid>
                  </Grid>
                  <Grid container>
                    <Grid item xs={6}>
                      ISBN-13
                    </Grid>
                    <Grid item xs={6}>
                      {book.isbn13}
                    </Grid>
                  </Grid>
                </Grid>
              </BookInfo>
              <BookDescription>
                <Typography variant="h6">Renha da editora</Typography>
                <img src="/img/icons/quotes.svg" alt="" />
                {book.description}
              </BookDescription>
            </ModalContent>
          </ModalContainer>
        </>
      )}
    </>
  );
};

export default BookModal;
