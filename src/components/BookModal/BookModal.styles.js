import { Container, Modal, Typography } from "@mui/material";
import { styled } from "@mui/material/styles";

export const ModalContainer = styled(Modal)`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const ModalContent = styled(Container)`
  background-color: ${({ theme }) => theme.palette.background.paper};
  padding: ${({ theme }) => theme.spacing(4)};
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: repeat(4, auto);
  grid-template-areas:
    "book title"
    "book authors"
    "book info"
    "book description";
  gap: 2rem;

  ${({ theme }) => theme.breakpoints.down("md")} {
    margin: 0 1rem;
  }

  ${({ theme }) => theme.breakpoints.down("sm")} {

    grid-template-columns: 1fr;
    grid-template-areas:
      "book"
      "title"
      "authors"
      "info"
      "description";
  }

  &.MuiContainer-root {
    max-width: ${({ theme }) => theme.spacing(118)};

    ${({ theme }) => theme.breakpoints.down("sm")} {
      max-width: ${({ theme }) => theme.spacing(62.5)};
    }
  }
`;

export const BookImg = styled("img")`
  grid-area: book;
  ${({ theme }) => theme.breakpoints.down("sm")} {
    justify-self: center;
  }
`;

export const BookTitle = styled(Typography)`
  grid-area: title;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const BookAuthors = styled(Typography)`
  grid-area: authors;
`;

export const BookInfo = styled("div")`
  grid-area: info;

  .MuiGrid-item:last-child {
    text-align: right;
    color: ${({ theme }) => theme.palette.text.secondary};
  }
`;

export const BookDescription = styled("div")`
  grid-area: description;
  color: ${({ theme }) => theme.palette.text.secondary};

  .MuiTypography-h6 {
    color: ${({ theme }) => theme.palette.text.primary};
  }
  img {
    width: ${({ theme }) => theme.spacing(3.5)};
    opacity: 0.6;
  }
`;

export const CloseIcon = styled("img")`
  cursor: pointer;
  width: ${({ theme }) => theme.spacing(4)};
  position: absolute;
  top: 2rem;
  right: 2rem;
`;
