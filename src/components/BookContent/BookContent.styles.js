import { styled } from "@mui/material/styles";
import { Card, CardContent } from "@mui/material";

export const BooksCard = styled(Card)`
  display: flex;
  justify-content: space-around;
  max-width: ${({ theme }) => theme.spacing(40)};
  height: ${({ theme }) => theme.spacing(27)};

  img {
    height: ${({ theme }) => theme.spacing(20)};
    margin: 1rem 0 1rem 1rem;
  }
`;

export const BookInfo = styled(CardContent)`
  padding: 0;
  margin: 1rem;
  font-size: ${({ theme }) => theme.spacing(15)};
  .MuiTypography-body2 {
    margin-bottom: ${({ theme }) => theme.spacing(3)};
  }
`;
