import React, { useEffect, useState } from "react";
import { CardActionArea, Typography } from "@mui/material";
import { BookInfo, BooksCard } from "./BookContent.styles";
import BookService from "../../services/BookService";

const BookContent = (props) => {
  const { page, amount, selectBook, setTotalItems } = props;
  const [books, setBooks] = useState(null);

  const findAllBooks = async (newPage) => {
    const { data, totalItems } = await BookService.findAllBooks(
      newPage + 1,
      amount
    );
    setBooks(data);
    setTotalItems(totalItems);
  };

  useEffect(() => {
    findAllBooks(page);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  return (
    <>
      {books?.length > 0 &&
        books.map((book) => (
          <CardActionArea key={book.id} onClick={() => selectBook(book.id)}>
            <BooksCard>
              <img src={book.imageUrl} alt={book.title} />
              <BookInfo>
                <Typography variant="body1">
                  <b>{book.title}</b>
                </Typography>
                <Typography variant="body2" color="text.author">
                  {book?.authors?.join(", ")}
                </Typography>
                <Typography variant="subtitle2" color="text.secondary">
                  {book.pageCount} páginas
                </Typography>
                <Typography variant="subtitle2" color="text.secondary">
                  Editora {book.publisher}
                </Typography>
                <Typography variant="subtitle2" color="text.secondary">
                  Publicado em {book.published}
                </Typography>
              </BookInfo>
            </BooksCard>
          </CardActionArea>
        ))}
    </>
  );
};

export default BookContent;
