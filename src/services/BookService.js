import { ApiService } from "./ApiService";

class BookService {
  async findAllBooks(page, amount = 12) {
    try {
      const resp = await ApiService.get("books", {
        params: {
          page,
          amount,
        },
      });
      return resp.data;
    } catch (error) {
      // eslint-disable-next-line no-console
      console.error("Erro ao buscar os livros", error.message);
      return error;
    }
  }

  async findBookById(bookId) {
    try {
      const resp = await ApiService.get(`books/${bookId}`);
      return resp.data;
    } catch (error) {
      // eslint-disable-next-line no-console
      console.error("Erro ao buscar livro");
      return error;
    }
  }
}

export default new BookService();
