import Cookies from "js-cookie";
import { ApiService } from "./ApiService";

class AuthService {
  constructor() {
    this.ACCESS_TOKEN = "access_token";
    this.REFRESH_TOKEN = "refresh_token";
  }

  async authentication(email, password) {
    try {
      const resp = await ApiService.post("/auth/sign-in", {
        email,
        password,
      });
      this.setAccessToken(resp.headers.authorization);
      this.setRefreshToken(resp.headers["refresh-token"]);
      return resp.data;
    } catch (error) {
      // eslint-disable-next-line no-console
      console.error("Email e/ou Senha incorretos");
      throw error;
    }
  }

  setAccessToken(accessToken) {
    Cookies.set(this.ACCESS_TOKEN, accessToken);
  }

  setRefreshToken(refreshToken) {
    Cookies.set(this.REFRESH_TOKEN, refreshToken);
  }

  getAccessToken() {
    return Cookies.get(this.ACCESS_TOKEN);
  }

  getRefreshToken() {
    return Cookies.get(this.REFRESH_TOKEN);
  }

  deleteAccessToken() {
    Cookies.remove(this.ACCESS_TOKEN);
  }

  deleteRefreshToken() {
    Cookies.remove(this.REFRESH_TOKEN);
  }
}

export default new AuthService();
