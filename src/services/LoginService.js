import Cookies from "js-cookie";
import AuthService from "./AuthService";

class LoginService {
  constructor() {
    this.USER_NAME = "user_name";
  }

  async login(email, password) {
    const user = await AuthService.authentication(email, password);
    return user;
  }

  logout() {
    AuthService.deleteAccessToken();
    AuthService.deleteRefreshToken();
  }

  setUserName(userName) {
    Cookies.set(this.USER_NAME, userName);
  }

  getUserName() {
    return Cookies.get(this.USER_NAME);
  }
}

export default new LoginService();
