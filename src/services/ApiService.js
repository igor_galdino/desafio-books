import axios from "axios";
import AuthService from "./AuthService";
import LoginService from "./LoginService";

const URL = "https://books.ioasys.com.br/api/v1";

// eslint-disable-next-line import/prefer-default-export
export const ApiService = axios.create({
  baseURL: URL,
  headers: {
    "Content-type": "application/json",
  },
});

ApiService.interceptors.request.use(async (config) => {
  const accessToken = AuthService.getAccessToken();
  if (accessToken) {
    // eslint-disable-next-line no-param-reassign
    config.headers.Authorization = `Bearer ${accessToken}`;
  }
  return config;
});

ApiService.interceptors.response.use(
  (response) => response,
  async (error) => {
    if (error.response.status === 401) {
      LoginService.logout();
      Promise.reject(error.config);
    }
    return Promise.reject(error);
  }
);
